import React, { useState } from 'react';
import axios from 'axios';
import "../css/signup.css"

const Signup = () => {
  const [credentials, setCredentials] = useState({ email: '', password: '' });

  const handleSignup = () => {
    const baseURL = 'https://geo.tidynodes.com';
    const signupEndpoint = `${baseURL}/api/signup`; 

    axios
      .post(signupEndpoint, credentials, {
        headers: {
          'Content-Type': 'application/json', 
        },
      })
      .then((response) => {
        console.log(response.data); 
      })
      .catch((error) => {
        console.error('Signup error:', error);
        console.error('Response:', error.response);
      });
  };

  return (
    <div className='form '>
 <h4 className='signup_txt'>Sign Up</h4>
      <br/>
      <input
        type="text"
        placeholder="Full Name"
        name="fullname"
        required
        onChange={(e) => setCredentials({ ...credentials, fullname: e.target.value })}
      />
      <br/>
      <br/>
      <input
        type="email"
        placeholder="Email"
        name="email"
        required
        onChange={(e) => setCredentials({ ...credentials, email: e.target.value })}
      />
      <br/>
      <br/>
      <input
        type="password"
        placeholder="Password"
        name="password"
        required
        onChange={(e) => setCredentials({ ...credentials, password: e.target.value })}
      />
      <br/>
      <br/>
      <input
        type="password"
        placeholder="Password 2"
        name="password2"
        required
        onChange={(e) => setCredentials({ ...credentials, password2: e.target.value })}
      />
      <br/>
      <br/>
      <input
        type="text"
        placeholder="Country"
        name="country"
        required
        onChange={(e) => setCredentials({ ...credentials, country: e.target.value })}
      />
      <br/>
      <br/>
      <input
        type="text"
        placeholder="zip"
        name="zip"
        required
        onChange={(e) => setCredentials({ ...credentials, zip: e.target.value })}
      />
      <br/>
      <br/>
      <input
        type="text"
        placeholder="phone"
        name="phone"
        required
        onChange={(e) => setCredentials({ ...credentials, phone: e.target.value })}
      />
      <br/>
      <br/>
      <input
        type="text"
        placeholder="billing_address"
        name="billing_address"
        required
        onChange={(e) => setCredentials({ ...credentials, billing_address: e.target.value })}
      />
      <br/>
      <br/>
      <button onClick={handleSignup} class="bn31" href="/"><span class="bn31span">Sign Up</span></button>
    </div>
  );
};

export default Signup;


    // return (
    //     <div>
    //         <div className="offset-lg-3 col-lg-6">
    //             <form className="container" onSubmit={handleSignup}>
    //                 <div className="card">
    //                     <div className="card-header">
    //                         <h1>User Registeration</h1>
    //                     </div>
    //                     <div className="card-body">

    //                         <div className="row">
    //                             <div className="col-lg-6">
    //                                 <div className="form-group">
    //                                     <label>User Name <span className="errmsg">*</span></label>
    //                                     <input value={id} onChange={e => idchange(e.target.value)} className="form-control"></input>
    //                                 </div>
    //                             </div>
    //                             <div className="col-lg-6">
    //                                 <div className="form-group">
    //                                     <label>Password <span className="errmsg">*</span></label>
    //                                     <input value={password} onChange={e => passwordchange(e.target.value)} type="password" className="form-control"></input>
    //                                 </div>
    //                             </div>
    //                             <div className="col-lg-6">
    //                                 <div className="form-group">
    //                                     <label>Full Name <span className="errmsg">*</span></label>
    //                                     <input value={name} onChange={e => namechange(e.target.value)} className="form-control"></input>
    //                                 </div>
    //                             </div>
    //                             <div className="col-lg-6">
    //                                 <div className="form-group">
    //                                     <label>Email <span className="errmsg">*</span></label>
    //                                     <input value={email} onChange={e => emailchange(e.target.value)} className="form-control"></input>
    //                                 </div>
    //                             </div>
    //                             <div className="col-lg-6">
    //                                 <div className="form-group">
    //                                     <label>Phone <span className="errmsg"></span></label>
    //                                     <input value={phone} onChange={e => phonechange(e.target.value)} className="form-control"></input>
    //                                 </div>
    //                             </div>
    //                             <div className="col-lg-6">
    //                                 <div className="form-group">
    //                                     <label>Country <span className="errmsg">*</span></label>
    //                                     <select value={country} onChange={e => countrychange(e.target.value)} className="form-control">
    //                                         <option value="india">India</option>
    //                                         <option value="usa">USA</option>
    //                                         <option value="singapore">Singapore</option>
    //                                     </select>
    //                                 </div>
    //                             </div>
    //                             <div className="col-lg-12">
    //                                 <div className="form-group">
    //                                     <label>Address</label>
    //                                     <textarea value={address} onChange={e => addresschange(e.target.value)} className="form-control"></textarea>
    //                                 </div>
    //                             </div>
    //                             <div className="col-lg-6">
    //                                 <div className="form-group">
    //                                     <label>Gender</label>
    //                                     <br></br>
    //                                     <input type="radio" checked={gender === 'male'} onChange={e => genderchange(e.target.value)} name="gender" value="male" className="app-check"></input>
    //                                     <label>Male</label>
    //                                     <input type="radio" checked={gender === 'female'} onChange={e => genderchange(e.target.value)} name="gender" value="female" className="app-check"></input>
    //                                     <label>Female</label>
    //                                 </div>
    //                             </div>

    //                         </div>

    //                     </div>
    //                     <div className="card-footer">
    //                         <button type="submit" className="btn btn-primary">Register</button> |
    //                         <Link to={'/login'} className="btn btn-danger">Close</Link>
    //                     </div>
    //                 </div>
    //             </form>
    //         </div>


    //     </div>
    // );
// }

// export default Signup;
// import React, { useState } from 'react';
// import axios from 'axios'; 
// import '../css/login.css';

// const Signup = () => {
//   const [credentials, setCredentials] = useState({ fullname: '', email: '', password: '' });

//   const handleSignup = () => {
//     const baseURL = 'https://geo.tidynodes.com';

//     const signUpendpoint = `${baseURL}/api/signup`;

//     axios
//     .post(signUpendpoint, credentials, {
//       headers: {
//         'Content-Type': 'application/x-www-form-urlencoded',
     
       
//       },
//     })
//     .then((response) => {
//     console.log(response)
//     })
//     .catch((error) => {
//       console.error('Signup error:', error);
//       console.error('Response:', error.response);
//     });
  
//   };

//   return (
//     <div>
//       <input
//         type="text"
//         placeholder="Full Name "
//         name="fullname"
//         onChange={(e) => setCredentials({ ...credentials, fullname: e.target.value })}
//       />
//       <input
//         type="email"
//         placeholder="Email"
//         name="email"
//         onChange={(e) => setCredentials({ ...credentials, email: e.target.value })}
//       />
//       <input
//         type="password"
//         placeholder="Password"
//         name="password"
//         onChange={(e) => setCredentials({ ...credentials, password: e.target.value })}
//       />
//       <button onClick={handleSignup}>Sign Up</button>
//     </div>
//   );
// };

// export default Signup;
