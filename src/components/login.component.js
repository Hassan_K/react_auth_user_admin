import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom'; 
import '../css/login.css';

const Login = () => {
  const [credentials, setCredentials] = useState({ email: '', password: '' });
  const navigate = useNavigate();

  const handleLogin = () => {
    const baseURL = 'https://geo.tidynodes.com';
    const loginEndpoint = `${baseURL}/api/login`;

    axios
      .post(loginEndpoint, credentials, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        console.log(response);
        
        navigate('/welcome');
      })
      .catch((error) => {
        console.error('Login error:', error);
        console.error('Response:', error.response);
      });
  };

  return (
    <div className='form_login'>
      <br/>
      <h1 className='login_txt'>Login</h1>
      <br />
      <input
        className='login_input'
        type="email"
        placeholder="Email"
        name="email"
        required
        onChange={(e) => setCredentials({ ...credentials, email: e.target.value })}
      />
      <br />
      <br />
      <input
        className='login_input'
        type="password"
        placeholder="Password"
        name="password"
        required
        onChange={(e) => setCredentials({ ...credentials, password: e.target.value })}
      />
      <br />
      <br />
      <br />
      <button onClick={handleLogin} class="bn311" href="/"><span class="bn311span">Login</span></button>
 

    </div>
  );
};

export default Login;
