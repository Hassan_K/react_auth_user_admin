import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom'; 
import '../css/userdata.css';


const WelcomePage = () => {
  const [userData, setUserData] = useState({});
const navigate = useNavigate()
  const authToken =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0cnlUb0d1ZXNzIjoidXdwWGJnYms5TnlNTEtzIiwiaWQiOjY4LCJlbWFpbCI6InNxQGdtYWlsLmNvbSIsInJvbGUiOiJfbFh5P0FJRk5ufXVxPnJ9RGJ-XmYqaylWfGo_ZUFpMn5ydHl9ZGpCQXRofFprc1FsciIsImlhdCI6MTY5NjQzODI3MywiZXhwIjoxNjk5MDMwMjczfQ.h4ksnvr7CLBfTQ3tIDbl4Awugt8EKbXJc3p1hMnivAk'; // Replace with your authentication token

  useEffect(() => {
    const apiUrl = 'https://geo.tidynodes.com/api/user';

    const headers = {
      Authorization: `Bearer ${authToken}`,
      auth: `${authToken}`,
    };

    axios
      .get(apiUrl, { headers })
      .then((response) => {
        setUserData(response.data);
      })
      .catch((error) => {
        console.error('Error fetching user data:', error);
      });
  }, [authToken]);

  const renderUserData = () => {
    if (!userData.USER_DATA) {
      return <p>Loading user data...</p>;
    }

    return (
      <div className=''>
   
        <div className="userdata">
          <h5>ID: {userData.USER_DATA.id }</h5>
          <br/>
          <h5>Full Name: {userData.USER_DATA.fullname }</h5>
          <br/>
          <h5>Email: {userData.USER_DATA.email}</h5>
          <br/>
          <h5>Country: {userData.USER_DATA.country }</h5>
          <br/>
          <h5>Zip: {userData.USER_DATA.zip }</h5>
          <br/>
          <h5>Phone: {userData.USER_DATA.phone }</h5>
          <br/>
          <h5>Billing Address: {userData.USER_DATA.billing_address }</h5>
          <br/>
          <h5>Api Key: {userData.USER_DATA.apiKey }</h5>
          <br/>
          <h5>Visits: {userData.USER_DATA.visits }</h5>
          <br/>
          <h5>Free_quota: {userData.USER_DATA.free_quota }</h5>
          <br/>
          <h5>Amount_owed : {userData.USER_DATA.amount_owed }</h5>
        </div>
        <br/>
        <br/>
        <br/>
        <button onClick={() => navigate('/new')} class="bn312_1">
  <span class="bn312_1span">Update Password</span>
</button>
      </div>
    );
  };

  return (
    <div>
      <h2 className='userinfo__txt'>User Info</h2>
     
      {renderUserData()}
    </div>
  );
};

export default WelcomePage;
