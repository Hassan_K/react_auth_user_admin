import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import '../css/login.css';

const NewComponent = () => {
  const [currentPassword, setCurrentPassword] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const authToken =
    // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0cnlUb0d1ZXNzIjoicjFRc1k2Sld5REhtYldBIiwiaWQiOjY4LCJlbWFpbCI6InNxQGdtYWlsLmNvbSIsInJvbGUiOiJfbFh5P0FJRk5ufXVxPnJ9RGJ-XmYqaylWfGo_ZUFpMn5ydHl9ZGpCQXRofFprc1FsciIsImlhdCI6MTY5NjUyNTA5NCwiZXhwIjoxNjk5MTE3MDk0fQ.0oqZE-KTsUlzkcVhfVj4JeOW09Vp0kwNH5-wr5kcTzM';

    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0cnlUb0d1ZXNzIjoiZVlQRWNqYlNwSXRpWHdxIiwiaWQiOjY5LCJlbWFpbCI6InNxYUBnbWFpbC5jb20iLCJyb2xlIjoiX2xYeT9BSUZObn11cT5yfURifl5mKmspVnxqP2VBaTJ-cnR5fWRqQkF0aHxaa3NRbHIiLCJpYXQiOjE2OTY4Njg3ODIsImV4cCI6MTY5OTQ2MDc4Mn0.rA4pZvfTEyRm8g76_zFweCbOx05S7xPxjo0Jr_e7qyQ"
  
    const navigate = useNavigate();

  const handleUpdatePassword = () => {
    // Check if passwords match
    if (password !== confirmPassword) {
      alert('Passwords do not match');
      return;
    }

    const baseURL = 'https://geo.tidynodes.com';
    const updatePasswordEndpoint = `${baseURL}/api/updatePass`;

    const data = {
      currentPass: currentPassword,
      newPass: password,
      newPass2: confirmPassword,
    };

    axios
      .post(updatePasswordEndpoint, data, {
        headers: {
          'Content-Type': 'application/json',
          auth: `${authToken}`,
        },
      })
      .then((response) => {
        console.log(response);
        alert('Password updated successfully');
        navigate('/welcome');
      })
      .catch((error) => {
        console.error('Password update error:', error);
        console.error('Response:', error.response);
        alert('Password update failed');
      });
  };

  return (
    <div className='form_login'>
      <br />
      <h1 className='login_txt'>Update Password</h1>
      <br />
      <input
        className='login_input'
        type="password"
        placeholder="Current Password"
        name="currentPass"
        required
        value={currentPassword}
        onChange={(e) => setCurrentPassword(e.target.value)}
      />
      <br />
      <input
        className='login_input'
        type="password"
        placeholder="New Password"
        name="newPass"
        required
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <br />
      <input
        className='login_input'
        type="password"
        placeholder="Confirm Password"
        name="newPass2"
        required
        value={confirmPassword}
        onChange={(e) => setConfirmPassword(e.target.value)}
      />
      <br />
      <br />
      <br />
      <br />
      <button onClick={handleUpdatePassword} className="bn312">
        <span className="bn312span">Update Password</span>
      </button>
    </div>
  );
};

export default NewComponent;
