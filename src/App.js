// App.js

import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import WelcomePage from './components/WelcomePage';
import Login from './components/login.component';
import SignUp from './components/signup.component';
import NewComponent from './components/NewComponent'; // Import the NewComponent

function App() {
  return (
    <Router>
      <br/>
      <br/>
      <div className="App">
        <div className="auth-wrapper">
          <div className="auth-inner">
            <Routes>
              <Route exact path="/" element={<Login />} />
              <Route path="/sign-in" element={<Login />} />
              <Route path="/sign-up" element={<SignUp />} />
              <Route path="/welcome" element={<WelcomePage />} />
              <Route path="/new" element={<NewComponent />} /> {/* Add the new route */}
            </Routes>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
